import java.util.Scanner;

public class Practice3 {
	public static class ReceiptTotal {
	    public static void main(String[] args) {
	    	Scanner console = new Scanner(System.in);
	    	double total = food(console);
	    	results(total);
	    	}
			
		}
	    	public static double food(Scanner console) {
	    	System.out.print("How many people ate? ");
	    	int people = console.nextInt();
	    	double subtotal = 0.0; 
	    	for (int i = 1; i <= people; i++) {
	    	System.out.print("Person #" + i +": How much did your dinner cost? ");
	    	double Cost = console.nextDouble();
	    	subtotal = subtotal + Cost; // add to sum
	    	}
	    	return subtotal;
	    }
	public static void results(double subtotal) {
		double tax = subtotal * .08;
		double tip = subtotal * .15;
		double total = subtotal + tax + tip;
		System.out.println("Subtotal: $" + subtotal);
		System.out.println("Tax: $" + tax);
		System.out.println("Tip: $" + tip);
		System.out.println("Total: $" + total);

		}	
	}

